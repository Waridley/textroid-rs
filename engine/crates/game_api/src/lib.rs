pub use bevy_app::*;
pub use bevy_core::*;
pub use bevy_ecs::*;
pub use bevy_property::*;

#[cfg(test)]
mod tests {
	#[test]
	fn it_works() {
		assert_eq!(2 + 2, 4);
	}
}
