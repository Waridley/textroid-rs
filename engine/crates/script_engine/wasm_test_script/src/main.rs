extern "C" {
    fn say_hello();
}

use bevy_property::prelude::*;

#[derive(Properties, Copy, Clone, Debug)]
#[repr(C)]
pub struct Foo {
    bar: i32,
    baz: f64,
}

static mut FOO: Foo = Foo { bar: 64, baz: std::f64::consts::PI };

#[no_mangle]
pub fn set_bar(value: i32) -> i32 {
    unsafe {
        FOO.set_prop("bar", &value);
        *FOO.prop_val::<i32>("bar").unwrap()
    }
}

#[no_mangle]
pub fn get_foo() -> *mut Foo {
    unsafe { &mut FOO }
}

#[no_mangle]
pub fn print_foo() {
    unsafe { dbg!(&FOO) };
}

#[no_mangle]
pub fn _update(num: i64) {
    println!("{}", num);
}

fn main() {
    unsafe { say_hello() };
    println!("Hello, Tallon IV!");
}