use wasmtime::{Store, Module, Instance, Engine, Linker, Config};
use wasmtime_wasi::{Wasi, WasiCtx};
use bevy::prelude::*;
use anyhow::Result;
use std::ops::{DerefMut, Deref};
use std::time::{Instant,};

const WASM_BYTES: &[u8] = include_bytes!(
	"../../wasm_test_script/bin/wasm32-wasi/debug/wasm_test_script.wasm"
);

#[test]
fn script_components() {
	let mut cfg = Config::new();
	let cfg = cfg
		.wasm_threads(true)
		.wasm_simd(true);
	
	let engine = Engine::new(cfg);
	
	let module = Module::new(&engine, WASM_BYTES).expect("create module");
	
	AppBuilder::default()
		.add_resource(module)
		.add_startup_system(run_setup_script.system())
		.add_startup_system(spawn_scripted_entities.system())
		.add_system(call_update.system())
		.run();
}

fn run_setup_script(
	_command: Commands,
	module: Res<Module>,
) {
	let linker = init_wasi_module(&*module).expect("init_wasi_module");
	let instance = linker.instantiate(&*module).expect("instantiate");
	
	// let start_fn = instance
	// 	.get_func("_start",)
	// 	.expect("get `_start` function",);
	// let result = start_fn.call(&[],);
	// println!("{:#?}", result);
}

struct ScriptInstance(Instance);

/// Safety: We must move everything attached to a `Store` to another thread
/// all at once. For now, I am making the Instance:Store relationship 1:1
unsafe impl Send for ScriptInstance {}
/// Safety: This is not actually Sync, but I'm only going to access it in
/// one system at a time, so wrapping it in a Mutex is pointless and I'm
/// already unsafely implementing Send, so why not.
unsafe impl Sync for ScriptInstance {}

impl Deref for ScriptInstance {
	type Target = Instance;
	
	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl DerefMut for ScriptInstance {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0
	}
}

fn spawn_scripted_entities(
	mut commands: Commands,
	module: Res<Module>,
) {
	let linker = init_wasi_module(&*module).expect("init_wasi_module");
	for _ in 1..10_000 {
		let instance = ScriptInstance(linker.instantiate(&*module).expect("instantiate"));
		commands.spawn((instance,));
	}
	println!("Spawned entities");
}

fn call_update(mut query: Query<(Entity, &ScriptInstance)>) {
	let start = Instant::now();
	for (entity, instance) in &mut query.iter() {
		let update_fn = instance
			.get_func("_update")
			.expect("get `_update` function");
		
		update_fn
			.call(&[wasmtime::Val::I64(entity.id() as i64)])
			.expect("call `_update`");
	}
	let duration = Instant::now().duration_since(start);
	println!("{:#?}", duration);
}



fn init_wasi_module(module: &Module) -> Result<Linker> {
	let store = Store::new(module.engine());
	let mut linker = Linker::new(&store);
	linker.func("env", "say_hello", || println!("Hello from Wasmtime host!"))?;
	let wasi_ctx = WasiCtx::new(std::env::args())?;
	let wasi = Wasi::new(&store, wasi_ctx);
	wasi.add_to_linker(&mut linker)?;
	
	Ok(linker)
}
