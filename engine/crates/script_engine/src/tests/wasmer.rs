use bevy::{
	app::AppBuilder,
	ecs::{Commands, Entity, IntoQuerySystem, Res},
};
use wasmer::{
	import_namespace, Function, Instance, InstantiationError, Module, Store,
	Val,
};
use wasmer_compiler_singlepass::Singlepass;
use wasmer_engine_jit::JIT;
use wasmer_wasi::WasiState;
use std::time::Instant;
use bevy::ecs::Query;
use std::ops::Deref;

const WASM_BYTES: &[u8] = include_bytes!(
	"../../wasm_test_script/bin/wasm32-wasi/debug/wasm_test_script.wasm"
);

#[test]
fn script_components() {
	let store = Store::new(&JIT::new(&Singlepass::default()).engine());
	let module = Module::new(&store, WASM_BYTES).expect("create Module");

	AppBuilder::default()
		.add_resource(module)
		.add_resource(store)
		.add_startup_system(run_setup_script.system())
		.add_startup_system(spawn_scripted_entities.system())
		.add_system(call_update.system())
		.run();
}

fn run_setup_script(
	_command: Commands,
	module: Res<Module>,
	store: Res<Store>,
) {
	let instance = init_wasi_script("test_script_instance", &*module, &*store)
		.expect("init_wasi_script");

	let start_fn = instance
		.exports
		.get_function("_start")
		.expect("get `_start` function",);
	let result = start_fn.call(&[]);
	println!("{:#?}", result);
}

struct ScriptInstance(Instance);

/// Safety: We must move everything attached to a `Store` to another thread
/// all at once. For now, I am making the Instance:Store relationship 1:1
unsafe impl Sync for ScriptInstance {}

impl Deref for ScriptInstance {
	type Target = Instance;
	
	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

fn spawn_scripted_entities(
	mut commands: Commands,
	module: Res<Module>,
	store: Res<Store>,
) {
	for _ in 1..10_000 {
		let instance = init_wasi_script("entity_script", &*module, &*store)
			.expect("init_wasi_script",);
		commands.spawn((ScriptInstance(instance),));
	}
	println!("Spawned entities");
}

fn call_update(mut query: Query<(Entity, &ScriptInstance)>) {
	let start = Instant::now();
	for (entity, instance) in &mut query.iter() {
		let update_fn = instance
			.exports
			.get_function("_update", )
			.expect("get `_update` function");
		
		update_fn
			.call(&[Val::I64(entity.id() as i64)])
			.expect("call `_update`");
	}
	let duration = Instant::now().duration_since(start);
	println!("{:#?}", duration);
}

fn init_wasi_script(
	name: &str,
	module: &Module,
	store: &Store,
) -> Result<Instance, InstantiationError> {
	let mut wasi_env =
		WasiState::new(name,).finalize().expect("build WasiState");
	let import_object = &mut wasi_env
		.import_object(module)
		.expect("create ImportObject");
	
	import_object.register("env", import_namespace! {
		{
			"say_hello" => Function::new_native(store, || println!("Hello from Wasmer host!"))
		}
	});
	
	let instance = Instance::new(module, import_object)?;
	wasi_env.set_memory(
		instance
			.exports
			.get_memory("memory")
			.expect("get memory")
			.clone()
	);
	Ok(instance)
}
