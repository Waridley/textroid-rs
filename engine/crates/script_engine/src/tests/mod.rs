pub mod wasmer;
pub mod wasmtime;




// use bevy::property::Properties;
// use wasmer::{
// 	import_namespace, Function, Instance, InstantiationError, Module, Store,
// 	ValueType, WasmPtr,
// };
// use wasmer_compiler_singlepass::Singlepass;
// use wasmer_engine_jit::JIT;
// use wasmer_wasi::WasiState;
//
// const WASM_BYTES: &[u8] = include_bytes!(
// 	"../../wasm_test_script/bin/wasm32-wasi/debug/wasm_test_script.wasm"
// );
//
// fn init_wasi_script(
// 	name: &str,
// 	module: &Module,
// 	store: &Store,
// ) -> Result<Instance, InstantiationError,> {
// 	let mut wasi_env =
// 		WasiState::new(name,).finalize().expect("build WasiState",);
// 	let import_object = &mut wasi_env
// 		.import_object(module,)
// 		.expect("create ImportObject",);
//
// 	import_object.register("env", import_namespace! {
// 		{
// 			"say_hello" => Function::new_native(store, || println!("Hello from Host!"))
// 		}
// 	},);
//
// 	let instance = Instance::new(module, import_object,)?;
// 	wasi_env.set_memory(
// 		instance
// 			.exports
// 			.get_memory("memory",)
// 			.expect("get memory",)
// 			.clone(),
// 	);
// 	Ok(instance,)
// }
//
// #[test]
// fn println_test() -> Result<(), Box<dyn std::error::Error,>,> {
// 	let store = Store::new(&JIT::new(&Singlepass::default(),).engine(),);
// 	let module = Module::new(&store, WASM_BYTES,)?;
//
// 	let instance = init_wasi_script("test_script_instance", &module, &store,)
// 		.expect("init_wasi_script",);
//
// 	let print_foo = instance
// 		.exports
// 		.get_native_function::<(), ()>("print_foo",)?;
// 	print_foo.call()?;
//
// 	let start = instance.exports.get_function("_start",)?;
// 	start.call(&[],)?;
//
// 	let set_bar = instance
// 		.exports
// 		.get_native_function::<i32, i32>("set_bar",)?;
// 	let result = set_bar.call(42,);
// 	dbg!(result)?;
//
// 	#[derive(Properties, Copy, Clone, Debug, Default)]
// 	#[repr(C)]
// 	struct Foo {
// 		bar: i32,
// 		baz: f64,
// 	}
//
// 	unsafe impl ValueType for Foo {}
//
// 	let get_foo = instance
// 		.exports
// 		.get_native_function::<(), WasmPtr<Foo,>>("get_foo",)?;
// 	let result = get_foo.call();
// 	let ptr = dbg!(result)?;
// 	let foo = unsafe {
// 		ptr.deref_mut(instance.exports.get_memory("memory",)?,)
// 			.expect("deref ptr",)
// 			.get_mut()
// 	};
// 	dbg!(&foo);
// 	foo.baz = std::f64::consts::E;
// 	dbg!(&foo);
//
// 	print_foo.call()?;
//
// 	Ok((),)
// }
