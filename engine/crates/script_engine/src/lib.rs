use wasmtime::{Instance, Module, Linker, Store};
use wasmtime_wasi::{Wasi, WasiCtx};
use anyhow::Result;
use std::convert::TryFrom;

#[cfg(test)] mod tests;

pub struct ScriptInstance(Instance);

pub struct Script {
	module: Module,
	linker: Linker,
}

impl Script {
	pub fn instantiate(&self) -> Result<ScriptInstance> {
		Ok(ScriptInstance(self.linker.instantiate(&self.module)?))
	}
}

impl TryFrom<Module> for Script {
	type Error = anyhow::Error;
	
	fn try_from(module: Module) -> Result<Script> {
		let store = Store::new(module.engine());
		let mut linker = Linker::new(&store);
		let wasi_ctx = WasiCtx::new(std::env::args())?;
		let wasi = Wasi::new(&store, wasi_ctx);
		wasi.add_to_linker(&mut linker)?;
		
		Ok(Script { module, linker, })
	}
}

impl AsRef<Module> for Script {
	fn as_ref(&self) -> &Module {
		&self.module
	}
}

impl AsMut<Linker> for Script {
	fn as_mut(&mut self) -> &mut Linker {
		&mut self.linker
	}
}