extern crate bevy_core;

use bevy_core::prelude::*;

fn main() {
	App::build().add_system(hello_world_system.system(),).run();
}

fn hello_world_system() {
	println!("Hello, World!")
}
