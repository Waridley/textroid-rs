
#[cfg(test)]
mod tests {
    extern crate bevy;
    use bevy::prelude::*;
    
    #[test]
    fn it_works() {
        App::build()
            .add_startup_system(setup.system())
            .run()
    }
    
    fn setup() {
        println!("Hello, World!")
    }
}
