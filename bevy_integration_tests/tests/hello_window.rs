mod common;
use crate::common::*;

fn main() {
	App::build()
		.add_default_plugins()
		.add_startup_system(hello_world_system.system())
		.run()
}

fn hello_world_system() {
	println!("Hello, default plugins!")
}
